let name = prompt('Enter your name');
let surname = prompt('Enter your surname');
let birthday = prompt('Enter you birth date in the following format dd.mm.yyyy');

function createNewUser(name, surname, birthday) {

    let birthdayArray = birthday.split('.');
    
    let newUser = {
        firstName: name,
        lastName: surname,
        birthday: new Date([birthdayArray[1], birthdayArray[0], birthdayArray[2]].join('.')),

        getAge() {
            let today = new Date();
            return Math.floor(new Date(today.getTime() - this.birthday.getTime())/(1000*3600*24*365));
        },

        getLogin() {
            return `${(this.firstName[0] + this.lastName).toLowerCase()}`;
        },

        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear());
        },

        inputData() {
            console.log(`My name is ${this.firstName} ${this.lastName}. I am ${this.getAge()} years old. My login is ${this.getLogin()}. The password is: ${this.getPassword()}`);
        },
      
    }

    return newUser.inputData();
   
}


console.log(createNewUser(name, surname, birthday));
/*
let kate = createNewUser(name, surname, birthday);
console.log(kate.getLogin());
console.log(kate.getAge());
console.log(kate.getPassword());*/